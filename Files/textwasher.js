var undoHistory = [];
var redoHistory = [];

function backUpText(text){
    if(text !== undefined || text !== null){
    undoHistory.unshift(text);
    }else if(text == undefined || text == null){
        console.log("Nothing to shift.");
    }
    console.log("Backed up: " + undoHistory);
}

function undo(){
    // console.log("Undo history contains:" + undoHistory);
    // console.log("Restoring: " + undoHistory[0]);
    if(undoHistory[0] !== undefined || undoHistory.length !== 0){
        document.getElementById("teliasArea").value = "";
        document.getElementById("teliasArea").value = undoHistory[0];
        redoHistory.unshift(undoHistory.shift());
    }else if(undoHistory[0] == undefined){
        console.log("Nothing to undo.");
    }
}

function redo(){
    if(redoHistory[0] !== undefined || redoHistory.length !== 0){
    document.getElementById("teliasArea").value = "";
    document.getElementById("teliasArea").value = redoHistory[0];
    undoHistory.unshift(redoHistory.shift());
}else if(redoHistory[0] == undefined){
    console.log("Nothing to redo.");
}
}

function deleteTelias() {
    var text = document.getElementById("teliasArea").value;
    backUpText(text);
    var newtext = text.replace(/telia A/g, "");
    var newertext = newtext.replace(/telia/g, "");
    document.getElementById("teliasArea").value = newertext;
}

function deleteSpaces() {
    var text = document.getElementById("teliasArea").value;
    backUpText(text);
    var newtext = text.replace(/ /g, "");
    document.getElementById("teliasArea").value = newtext;
}

function deleteDigits() {
    var text = document.getElementById("teliasArea").value;
    backUpText(text);
    for (i = 0; i < 10; i++) {
        var text = document.getElementById("teliasArea").value;
        var newtext;
        // console.log("digit = " + i);
        var is = i.toString();
        console.log(is);
        var regex = new RegExp(is, "g");
        console.log(regex);
        var newtext = text.replace(regex, "");
        console.log(newtext);
        document.getElementById("teliasArea").value = newtext;
    }
}

function deleteWhatever(event) {
    if (event.keyCode == 13) {
        deleteThis();
        console.log("Enter pressed (keycode " + event.keyCode + ")");
    } else { console.log("Press enter to delete characters"); }
}

function deleteThis(){
    var input = document.getElementById("deleteInput").value;
    var text = document.getElementById("teliasArea").value;
    backUpText(text);
    var regex = new RegExp(input, "g");
    var newtext = text.replace(regex, "");
    if (input !== "") {
        document.getElementById("teliasArea").value = newtext;
        console.log("All cases: " + input + " deleted.");
    } else if (input == "") {
        console.log("No input.");
    }
}

function replaceWhatever(event) {
    if (event.keyCode == 13) {
        replaceThis();
        console.log("Enter pressed (keycode " + event.keyCode + ")");

    } else { console.log("Press enter to delete characters"); }

}

function replaceThis(){
    var tobereplaced = document.getElementById("deleteInput").value;
    var toreplace = document.getElementById("replaceInput").value;
    var text = document.getElementById("teliasArea").value;
    backUpText(text);
    var regex = new RegExp(tobereplaced, "g");
    var newtext = text.replace(regex, toreplace);
    if (tobereplaced !== "") {
        document.getElementById("teliasArea").value = newtext;
        console.log("All cases: " + tobereplaced + " replaced with " + toreplace +".");
    } else if (tobereplaced == "") {
        console.log("No input.");
    }
}

function KeyPress(e) {
    var evtobj = window.event? event : e
    if (evtobj.keyCode == 90 && evtobj.ctrlKey){
        undo();
    };
}

document.onkeydown = KeyPress;